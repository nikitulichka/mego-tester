<div class="zForm zNice">
	<form>
		<div class="zForm-title">
			Название формы
		</div>
		<div class="zForm-row">
			<label>
				<span class="zForm-text">Текст</span>
				<span class="zForm-input"><input type="text" name="inp1" paleholder="Инпут текст" required="required" /></span>
			</label>
		</div>
		<div class="zForm-row rightFormat">
			<label>
				<span class="zForm-col zForm-text">Текст</span>
				<span class="zForm-col"><input type="text" name="inp2" paleholder="Инпут текст" required="required" /></span>
			</label>
		</div>
		<div class="zForm-row LeftFormat">
			<label>
				<span class="zForm-col"><input type="text" name="inp5" paleholder="Инпут текст" required="required" /></span>
				<span class="zForm-col zForm-text">Текст</span>
			</label>
		</div>
		<div class="zForm-row">
			<input type="file" name="inp3" title="Загрузить" required="required" />
		</div>
		<div class="zForm-row">
			<input type="text"  class="myclass" value="" placeholder="Кол-во" name="input1" required="required" data-ztype="qInput"/>
		</div>
		<div class="zForm-row">
			<input type="email" class="myclass" value="" name="input2" required="required" />
		</div>
		<div class="zForm-row">
			<select name="select1" class="myclass1">
				<option data-color="#f00">1</option>
				<option data-image="images/t/formIcon1.png">2</option>
				<option>3</option>
			</select>
		</div>
		<div class="zForm-row">
			<select disabled name="select1" class="myclass2"><option>1</option><option>1</option><option>1</option></select>
		</div>
		<div class="zForm-row">
			<span class="zForm-col">
				<label><input type="checkbox" class="myclass" value="" name="input4" required="required" /></label>
			</span>
			<span class="zForm-col">
				<input type="text" class="myclass" phoneRU="phoneRU" value="" name="input8" required="required" data-image="images/t/formIcon1.png" />
			</span>
		</div>
		<div class="zForm-row">
			<label><input type="radio" class="myclass" value="1" name="input5" required="required" /><span class="zForm-text">Текст</span></label>
			<label><input type="radio" class="myclass" value="2" name="input5" required="required" /><span class="zForm-text">Текст</span></label>
			<label><input type="radio"  class="myclass" value="3" name="input5" required="required" /><span class="zForm-text">Текст</span></label>
		</div>
		<div class="zForm-row">
			<select multiple="multiple" data-placeholder="Это multiple" required="required">
				<option>Это 1</option>
				<option>Это 2</option>
				<option>Это 3</option>
				<option>Это 4</option>
				<option>Это 5</option>
				<option>Это 6</option>
				<option>Это 7</option>
			</select>
		</div>
		<div class="zForm-row zForm-center">
			<input type="submit"  value="Отправить"  />
		</div>
		<div class="zForm-row">
			<input type="reset"  value="reset"  />
		</div>
	</form>
</div>