<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
	<script src="js/jquery-1.10.0.min.js" type="text/javascript"></script>

	<script src="js/selectivizr-min.js" type="text/javascript"></script>
	<script src="js/modernizr.js" type="text/javascript"></script>
	<script src="js/jquery.watermark.min.js" type="text/javascript"></script>

	<link href="css/font-awesome.css" rel="stylesheet" />
	
	<link href="css/bootstrap.css" rel="stylesheet" />
	<script src="js/bootstrap.js" type="text/javascript"></script>

	<script src="js/scr.js" type="text/javascript"></script>

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<script>
			document.createElement('header');
			document.createElement('nav');
			document.createElement('section');
			document.createElement('article');
			document.createElement('aside');
			document.createElement('footer');
			document.createElement('time');
		</script>	
		<script src="js/pie.js" type="text/javascript"></script>
		<script src="js/css3-mediaqueries.js"></script>
	<![endif]-->

	<!--[if lt IE 8]><script src="js/oldie/warning.js"></script><script>window.onload=function(){e("js/oldie/")}</script><![endif]-->
</head>
	<body>
		<div class="zHiddenBlock">
			
		</div>
		<div class="topBg">
			<img src="images/top_bg.png" alt="" /><span class="vfix"></span>
		</div>
		<div class="container-fluid">
			<header class="header">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 text-center">
						<!-- logo -->
						<a href="#" class="">
							<img src="images/logo.png" alt=""/>
						</a>
						<!-- /logo -->
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 text-right">
						<!-- navmemu -->
						<div class="tMenu navbar">
							<div class="navbar-header text-left">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#respMenu">
									<span class="sr-only">Открыть навигацию</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="collapse navbar-collapse" id="respMenu">
								<ul class="nav navbar-nav">
									<li><a href="#">Home</a></li>
									<li class="active"><a href="#">About Us</a></li>
									<li><a href="#">Services</a></li>
									<li><a href="#">Features</a></li>
									<li><a href="#">Shortcodes</a></li>
									<li><a href="#">Categories</a></li>
									<li><a href="#">Login</a></li>
									<li><a href="#">Contact Us</a></li>
								</ul>
							</div>
						</div>
						<!-- navmemu -->
					</div>
					<div class="clear"></div>
				</div>
			</header>
		</div>

		<?//if ($act=='index') include 'partials/banner.php';
			//else include 'partials/hsep.php';
		?>
